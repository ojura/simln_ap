Code example for beeclust in a two-arena setup, each arena has 2 casus that do 
self-reinforcement.

To run, use assisipy_utils -> exec_sim_timed. 

e.g.
cd speeds
exec_sim_timed -c v120_v240.conf -r 1

# Code organisation:

## common code -- runtime
  bees/     # bee agent simulation model and setup files
  robots/   # robot/casu controllers and libraries
  env/      # simulation configuration, and spawning walls/agents

## common code -- analysis or external monitoring
  observe/  # runtime monitoring (only of bee population so far)

## specific to "speeds" experiments
- these are templates for how to investigate the limits of timescale differences
  between coupled populations.

  dep_speeds/   # deployment files
  conf/         # parameter files for the casu/robots
  robots/       # (just a symlink to the parent robot dir)

## specific to "surrogates" experiments
- these [are] templates for investigations into split/distal populations,
  and how they can act in a similar or dissimilar manner to a single larger 
  population.



# How to change parameters

topology => a nbg file
casu parameters => conf/<>.conf
(new nodes => nbg, dep and arena file)

bee parameters => bees/<>.conf

bee population size => top.conf
arena locations => top.conf











