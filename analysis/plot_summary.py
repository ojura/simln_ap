import numpy as np
import matplotlib.pyplot as plt


# load data
X240 = np.loadtxt('data/slow150.txt', usecols=(1,2,3,4), delimiter=';')
X120 = np.loadtxt('data/osc150_120samples.txt', usecols=(1,2,3,4), delimiter=';')
X60  = np.loadtxt('data/osc_150.txt', usecols=(1,2,3,4), delimiter=';')
X20 = np.loadtxt('data/osc150_20samples.txt', usecols=(1,2,3,4), delimiter=';')

plt.figure(1); plt.clf()
plt.boxplot([X240[:,3], X120[:,3], X60[:,3], X20[:,3]], labels=["120s", "60s,", "30s", "10s"])
plt.title('oscillation index')

plt.figure(2); plt.clf()
plt.boxplot([X240[:,2], X120[:,2], X60[:,2], X20[:,2]], labels=["120s", "60s,", "30s", "10s"])
plt.title('Coll decn index')

# look at correlation if necessary.
plt.figure(3); plt.clf()
fig, ax = plt.subplots(2,2, num=3)
XX = [X20, X60, X120, X240]
labels=["120s", "60s,", "30s", "10s"][::-1]
for i in xrange(4):
    a = ax.flat[i]
    X = XX[i]
    a.scatter(X[:,2], X[:,3])
    a.set_title(labels[i])

plt.show()
