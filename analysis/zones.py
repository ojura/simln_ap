import numpy as np

#{{{ rolling_avg
def rolling_avg(x, n):
    '''
    given the sample x, provide a rolling average taking n samples per data point.
    NOT a quick solution, but easy...
    '''
    y = np.zeros((len(x),))
    for ctr in range(len(x)):
        y[ctr] = np.sum(x[ctr:(ctr+n)])

    return y/n
#}}}

def compute_dist_to_point(d, (x,y)):
    dx = (d[:,0] - x)**2.0
    dy = (d[:,1] - y)**2.0
    dd = np.sqrt(dx + dy)
    return dd

#{{{ segment_zone_domination
def segment_zone_domination(zc_north, zc_south, cross_comms=False, t_s=0, t_e=0,
                            verb=True):
    '''
    given a subset of data, compute the dominance of four zones, and also the joint
    '''
    prop_n = zc_north.sum(axis=1) / zc_north.sum() * 100
    prop_s = zc_south.sum(axis=1) / zc_south.sum() * 100

    # simple fraction of zones.
    nr =  ", ".join(["{:.1f}".format(i) for i in prop_s ])
    sr =  ", ".join(["{:.1f}".format(i) for i in prop_n ])
    # display something we can extract and parse easily
    s = ", ".join([str(t_s), nr, sr])
    if verb:
        print ("North - L|nmz|R for slice {}:{}s: ".format(t_s, t_e)), nr
        print ("South - L|nmz|R for slice {}:{}s: ".format(t_s, t_e)), sr

    print s

    if cross_comms:
        # to combine, we reverse the order of prop_X_n and add it to prop_X_s
        prop_all  = (prop_n[::-1]     + prop_s) / 2.0
        #all_zc    = (zc_north[::-1,:] + zc_south[:, :]) / 2.0
    else:
        prop_all  = (prop_n[::+1]     + prop_s) / 2.0
        #all_zc    = (zc_north[::+1,:] + zc_south[:, :]) / 2.0


    if verb:
        print "Combined -- with cross={}".format(cross_comms)
        print ("  L|nmz|R for slice {}:{}s:".format(t_s, t_e) +
            "\t".join(["{:.1f}".format(i) for i in prop_all ]))

    #return prop_n, prop_s, prop_all, all_zc
    return prop_n, prop_s, prop_all, s
#}}}

#{{{ compute_time_in_zones
def compute_time_in_zones(sd, fixed_points):
    # this doesn't really need nums or idx_ofs since we are only indexing into
    # an array from zero.
    n, m, _ = sd.shape
    distances = np.zeros((n, m, len(fixed_points)))

    for i in xrange(n):
        # extract the sampled data (just a handle)
        one_sd = sd[i, :, :]

        for j, pt in enumerate(fixed_points):
            # compute each distance
            distances[i, :, j] = compute_dist_to_point(one_sd[:,1:3], pt)

    return distances

def compute_time_in_circ_zones(sd, fixed_points, rad_threshold):
    # this doesn't really need nums or idx_ofs since we are only indexing into
    # an array from zero.
    n, m, _ = sd.shape
    distances = np.zeros((n, m, len(fixed_points)))

    for i in xrange(n):
        # extract the sampled data (just a handle)
        one_sd = sd[i, :, :]

        for j, pt in enumerate(fixed_points):
            # compute each distance
            distances[i, :, j] = compute_dist_to_point(one_sd[:,1:3], pt)

    zone_counts = (distances < rad_threshold).sum(axis=0)
    zc_other = n - zone_counts.sum(axis=1)
    all_zc = np.vstack((zone_counts[:,0], zc_other, zone_counts[:,1]))

    return all_zc



def compute_time_in_horiz_zones(sd, fixed_points, horiz_threshold):
    NC1x = fixed_points[0][0]
    NC2x = fixed_points[1][0]

    max_len = sd.shape[1] # cleaner way to compute sizes?
    n = sd.shape[0]
    zones     = np.zeros((n, max_len, len(fixed_points)) )

    for i in xrange(n):
        # extract the sampled data (just a handle)
        one_sd = sd[i, :, :]
        # consider distance only in x, and with a threshold too
        zones[i, :, 0] = (one_sd[:, 1] < (NC1x + horiz_threshold)).astype(int)
        # count number that are right of the fixed pt
        zones[i, :, 1] = (one_sd[:, 1] > (NC2x - horiz_threshold)).astype(int)
        # remainders - as vector afterwards

    zone_counts = zones.sum(axis=0)
    zc_other = n - zone_counts.sum(axis=1)
    all_zc = np.vstack((zone_counts[:,0], zc_other, zone_counts[:,1]))

    return all_zc

def compute_time_in_vert_zones(sd, fixed_points, vert_threshold):
    NC1y = fixed_points[0][1]
    NC2y = fixed_points[1][1]

    max_len = sd.shape[1] # cleaner way to compute sizes?
    n = sd.shape[0]
    zones     = np.zeros((n, max_len, len(fixed_points)) )

    for i in xrange(n):
        # extract the sampled data (just a handle)
        one_sd = sd[i, :, :]
        # top half: is it above the 2nd point - thresh?
        zones[i, :, 0] = (one_sd[:, 2] > (NC2y - vert_threshold)).astype(int)
        # count number that are below the fixed pt
        zones[i, :, 1] = (one_sd[:, 2] < (NC1y + vert_threshold)).astype(int)
        # remainders - as vector afterwards

    zone_counts = zones.sum(axis=0)
    zc_other = n - zone_counts.sum(axis=1)
    all_zc = np.vstack((zone_counts[:,0], zc_other, zone_counts[:,1]))

    return all_zc

#}}}

#{{{ gen_stackplot_of_zones
def pre_filtered_zone_stackplot(ax, all_zc, times, horiz_threshold=None,
                                n=None, colors=(
                                    (0.89, 0.13, 0.090),
                                    (0.98, 0.72, 0.099),
                                    (0.086, 0.34, 0.976)),
                                sample_dt=1.0, horiz_span=0, jump_tol=0,
                                stack_plot_avail=True,
                           ):
    if not (stack_plot_avail):
        ax.plot(times/60.,
                    all_zc[0,:],
                    color='k')
        ax.fill_between(times/60.,
                    all_zc[0,:],
                    color=colors[0], )
        ax.set_ylabel('#bees in W zone ({} cm from casu)'.format(horiz_threshold))
    else:

        ax.stackplot(times/60.,
                all_zc[0,:],
                all_zc[1,:],
                all_zc[2,:],
                colors=colors,
                #colors=((0.965, .363, 0.348), (0.98, 0.72, 0.099), (0.086, 0.34, 0.976))
                #colors=((0.89, 0.13, 0.090), (0.98, 0.72, 0.099), (0.086, 0.34, 0.976))
                )
        if horiz_threshold is not None:
            ax.set_ylabel('#bees per zone ({} cm from casu)'.format(horiz_threshold))

    ax.set_xlabel('time (m)')
    if n is not None:
        ax.set_ylim(0, n)
    #plt.xlim(0, 20) # for now just look at 20mins
    #ax.set_title('ctrl zone: {}cm, sample_dt: {}s, filter jumps >{:.1f}cm'.format(
    #    horiz_span - 2*horiz_threshold, sample_dt, jump_tol,
    #    ))

def gen_stackplot_of_zones(ax, all_zc, times, smoothing_scale, horiz_threshold,
                           n, colors=(
                               (0.89, 0.13, 0.090),
                               (0.98, 0.72, 0.099),
                               (0.086, 0.34, 0.976)),
                           sample_dt=1.0, horiz_span=0, excl_list=[], jump_tol=0,
                           ):

    ax.stackplot(times/60.,
            rolling_avg(all_zc[0,:], smoothing_scale),
            rolling_avg(all_zc[1,:], smoothing_scale),
            rolling_avg(all_zc[2,:], smoothing_scale),
            colors=colors,
            #colors=((0.965, .363, 0.348), (0.98, 0.72, 0.099), (0.086, 0.34, 0.976))
            #colors=((0.89, 0.13, 0.090), (0.98, 0.72, 0.099), (0.086, 0.34, 0.976))
            )
    ax.set_ylabel('#bees per zone ({} cm from casu)'.format(horiz_threshold))
    ax.set_xlabel('time (m)')
    ax.set_ylim(0, n)
    #plt.xlim(0, 20) # for now just look at 20mins
    ax.set_title('smooth:{}s, ctrl zone: {}cm, sample_dt: {}s, exclude: {}, filter jumps >{:.1f}cm'.format(
        sample_dt*smoothing_scale, horiz_span - 2*horiz_threshold, sample_dt,  excl_list, jump_tol,
        ))
#}}}

