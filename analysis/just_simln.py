'''
for a simulation-only experiment, run with assisipy-utils/exec_sim_timed
compute the collective decision levels left/right for each arena.
Time budget as well.

'''

#{{{ imports
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
import argparse, os, yaml, fnmatch
from datetime import datetime
import zones
from cbtb.logs import process_logs
from cbtb.bees import log_proc

import cbtb.maths
import cbtb.utils

#TODO: remove dependency on datetime?

#}}}

#{{{ compute oscillation index
def quanitfy_oscil(bleft, i_start, i_stop=None, scaling=60):
    '''
    compute the "energy" of signal, defined as deviations from the mean/DC value,
    and normalise according to sample length.
    '''
    bb = bleft[i_start:i_stop] # a fraction of bees on one side, for obs period
    bb = bb - bb.mean() # remove DC component

    # normalise according to duration ASSUMES SECONDS INTERVAL!!
    if i_stop == None:
        i_end = len(bleft)
    else:
        i_end = i_stop
    dur = i_end - i_start

    # compute normalised energy in observation period
    #rms = ((bb**2).sum()**0.5 )/ float(dur)
    oscil_power = ((bb**2).sum() ) / float(dur) * scaling
    return oscil_power

#}}}

#{{{ show fft
def show_fft(data, ax=None, use_helper=False , time_step=1.0):
    if ax is None:
        ax = plt.gca()

    if use_helper:
        freqs = np.fft.fftfreq(data.size, time_step)
        ps = np.abs(np.fft.fft(data))**2
        idx = np.argsort(freqs)
        ax.plot(freqs[idx], ps[idx])
        ax.set_xlabel('Hz')
        ax.set_xlim(xmin=0)
    else:
        freqs = np.fft.fft(data)
        ax.plot(np.abs(freqs)[:len(freqs)/2])
        ax.set_xlabel('arbitrary units...')
        ax.set_xlim(xmin=0)


    return ax
#}}}


#{{{ show_posns
def show_posns(sd, fignum=12, i_start=0, cols=2):
    rows = (sd.shape[0] +1)/cols # int divsn.
    plt.figure(fignum); plt.clf()
    fig, ax = plt.subplots(rows, cols, num=fignum, sharex=True, sharey=True)
    for i in xrange(sd.shape[0]):
        one_sd = sd[i, i_start:,:]
        ax.flat[i].plot(one_sd[:,1], one_sd[:,2], alpha=0.6)

    x = sd[:, i_start:, 1]
    y = sd[:, i_start:, 2]
    #xm = x.mean(axis=0)
    #ym = y.mean(axis=0)
    xm = x.flatten()
    ym = y.flatten()
    xx = np.random.randn(xm.shape[0],) *0.15 + np.ones_like(ym) * (y.min()-1)
    yy = np.random.randn(ym.shape[0],) *0.15 + np.ones_like(xm) * (x.min()-1)
    #yy = np.ones_like(xm) * (x.min()-1)
    ax.flat[0].scatter(yy, ym, alpha=0.07)
    ax.flat[2].scatter(xm, xx, alpha=0.07)

    return ax


#}}}
#{{{ read config
def read_config(cfg_file, rpt=None):
    with open(cfg_file) as f:
        cfg = yaml.safe_load(f)

    _pth = cfg.get('expt', None)
    if _pth is None:
        raise RuntimeError
    _rpt =  _pth['rpt']
    if rpt is None:
        rpt = _rpt
    label = "{}rpt{}".format(_pth['lbl'], rpt)

    base = os.path.join( _pth['base'], _pth['subgrp'], label)
    base = os.path.abspath(os.path.expanduser(base)) # handle ~ in paths
    cfg['base'] = base
    cfg['label'] = label
    cfg['rpt'] = rpt


    proj = None
    if base:
        # find the proj file
        deparch = os.path.join(base, 'archive', 'dep')
        for fn in os.listdir(deparch):
            if fnmatch.fnmatch(fn, "*.assisi"):
                proj = os.path.join(deparch, fn)

        # find the data directory
        datadir = None
        for fn in os.listdir(base):
            if fnmatch.fnmatch(fn, 'data_*'):
                print "[I] found data dir: {}".format(fn)
                datadir = fn
                break

    cfg['projfile'] = proj
    cfg['datadir']  = datadir

        #if not found:
        #    return None


    return cfg, base

def which_arena(proj_file, search_casu):
    '''
    find which arena the casu is in, within the assisi project proj
    '''

    #proj_file = os.path.join(p_root, depdir, pf)
    proj_dir = os.path.dirname(os.path.abspath(proj_file))
    with open(proj_file) as _f:
        project = yaml.safe_load(_f)

    af = project.get('arena')
    print "Arena file is ", af
    with open(os.path.join(proj_dir, af)) as _f:
        arena = yaml.safe_load(_f)

    for layer in sorted(arena):
        for _casu in arena[layer]:
            if _casu == search_casu:
                return layer

    # not found = return none.
    return None
#}}}

from enum import Enum

class Decision(Enum):
  NORTH = 1
  UND_NORTH = 2
  UND_SOUTH = 3
  SOUTH = 4

def to_decision(value, thresh_south, middle, thresh_north):
  D = Decision
  if value <= thresh_south:
    return D.SOUTH
  elif value <= middle:
    return D.UND_SOUTH
  elif value < thresh_north:
    return D.UND_NORTH
  elif value >= thresh_north:
    return D.NORTH
  else:
    assert(False)

def rcdi(decisions):
  D = Decision

  run_length = 0
  last_decision = None
  award_south = 0
  award_north = 0
  full_award = 0.
  full_run_length = 0
  for i in xrange(len(decisions)):
    current = decisions[i]

    # compute this just for normalization
    full_run_length += 1
    full_award += full_run_length

    if current == last_decision:
      run_length += 1
      if current == D.NORTH:
        award_north += run_length
      else:
        assert(current == D.SOUTH)
        award_south += run_length
    else:
      if last_decision == D.NORTH and current == D.UND_NORTH:
        pass
      elif last_decision == D.NORTH and current == D.UND_SOUTH:
        run_length = 0
      elif last_decision == D.SOUTH and current == D.UND_SOUTH:
        pass
      elif last_decision == D.SOUTH and current == D.UND_NORTH:
        run_length = 0
      elif current == D.NORTH or current == D.SOUTH:
          last_decision = current
          run_length = 1
          if current == D.NORTH:
            award_north += run_length
          else:
            assert(current == D.SOUTH)
            award_south += run_length
      else:
        assert(last_decision == None)

  #print "Award_south/full length: {}/{}".format(award_south, full_award)
  #print "Award_north/full length: {}/{}".format(award_north, full_award)

  return abs(award_north + award_south) / float(full_award)

if __name__ == '__main__':
    #{{{ args and config file
    parser = argparse.ArgumentParser()
    parser.add_argument('--conf', type=str, default=None)
    #parser.add_argument('-i', '--idx', type=int, default=1)
    #parser.add_argument('-r', '--rpt', type=int, default=None)
    parser.add_argument('-o', '--vid-offset', type=float, default=0.0,
                        help="how many seconds is the video start before the sync log (or -ve if video starts later)")
    parser.add_argument('-a', '--A', type=int, default=1)
    parser.add_argument('-si', '--start_index', type=int, default=600, help="define the analysis start time in sec")
    parser.add_argument('-s', '--emit-summary', action='store_true')
    parser.add_argument('-v', '--vert-arena',   action='store_true')
    parser.add_argument('-ci', '--casu-inspect', type=int, default=None)
    parser.add_argument('-r', '--rpt', type=int, default=None,
                        help="if none, read from file. this takes precedence")
    parser.add_argument('-bp', '--bee-prefix', type=str,
            default="bee_track-bee")
    parser.add_argument('-n', '--num-bees', type=int, default=5,
                        help='number of bees in each arena to read data for')
    args = parser.parse_args()

    #A = 1 ; casus = [1,2]
    if args.A == 0:   casus = [1,2]
    elif args.A == 1: casus = [4, 5]
    elif args.A == 2: casus = [7, 8]
    elif args.A == 3: casus = [6, 9]

    SHOW_SYNCFLASHES = False

    # read config
    cfg, basepath = read_config(args.conf, args.rpt)
    grp = cfg['groups'][args.A]
    #}}}

    #{{{ read data from casus
    irs   = []
    thrs  = []

    nchannels = 6

    #{{{ read IR & syncflash
    print "\n",
    print "=" * 60
    print "[I] reading IR & syncflash data for casus ", casus
    print "=" * 60

    print cfg['projfile']

    for c in casus:
        cname = "casu-sim-{:03d}".format(c)
        # which arena, where is data?
        arena = which_arena(cfg['projfile'],  cname)
        logpth = os.path.join(basepath, cfg['datadir'], arena, cname)
        print "[I] looking for casu {} in arena {} with path\n\t{}".format(
            cname, arena, logpth)

        # NOTE: no longer read sync data here, since we only want a single
        # global source for each video -- casu-001
        #_sync_lines = process_logs.read_syncflash(cname, logpth)
        #sync = np.loadtxt(_sync_lines, usecols=(0,1,), delimiter=';')
        #syncs.append(sync)

        ir_lines = process_logs.read_irs(cname, logpth)
        _d = np.loadtxt(ir_lines, usecols=xrange(1,8), delimiter=';')
        irs.append(_d)
        pre_skip = 10
        steps    = 50
        ir = _d[:, 1:]
        thr = np.zeros((nchannels,))
        for i in xrange(nchannels):
            ir_raw = ir[:,i]
            _thr = process_logs.guess_ir_thresh(ir_raw, gain=1.1, pre_skip=pre_skip, steps=steps)
            thr[i] = _thr
        thrs.append(thr)
    #}}}

    #{{{ read sync-flash refernce time
    ref_cname = cfg['sync']['ref_cname']
    arena = which_arena(cfg['projfile'],  ref_cname)
    logpth = os.path.join(basepath, cfg['datadir'], arena, ref_cname)
    print "\n",
    print "=" * 60
    print "[I] reading reference time (from {})".format(ref_cname)
    print "=" * 60

    # find sync start_time - use casu-001 if used?
    #cname = "casu-{:03d}".format(casus[0])
    found = False
    for fn in os.listdir(logpth):
        if fnmatch.fnmatch(fn, '{}*.sync.log'.format(ref_cname)):
            print "[I] found sync log {} -> {}".format(ref_cname, fn)
            found = True
            break
    # read the first line
    if found:
        with open(os.path.join(logpth, fn)) as f:
            sync_comment = f.readline().strip()

        if sync_comment.startswith('#') is True:
            sync_start_time = float(sync_comment.split()[-1])

        print "[I] reference time is {} ({})".format(
            sync_start_time, cbtb.utils.ux_ts_to_readable(sync_start_time))

    else:
        raise RuntimeError("[F] no reference time! ")

    # now read the main data
    _sync_lines = process_logs.read_syncflash(ref_cname, logpth)
    sync = np.loadtxt(_sync_lines, usecols=(0,1,), delimiter=';')
    #}}}

    #}}}

    #{{{ now read the bee posn data
    #{{{ setup parameters
    casu_idx_ofs = 0
    smoothing_scale = 5 # noisy data isn't partiularly useful for telling us about trends
    show_elementary = False
    #horiz_span = 8.0 # NEEd this to come from data!!! FIXME
    #horiz_threshold = (horiz_span / 2)  - 0.0
    ctrl_zone_width = 0.0
    dec_frac = 0.67
    show_sync = True

    sample_dt = 1.0


    top_idx = args.num_bees
    fig_offset = 0
    zf_for_zones = False # select zone or binom test output
    show_figs = True
    args.todraw = {
            'raw_aggs'       : True,
            'simplif_aggs'   : True,
            'metrics_v_time' : True,
            'tests_v_time'   : True,
            }


    # extract some variables / params from the config file
    path          = cfg['base']
    bee_prefix = "{}{}{}".format(
        cfg['simbees']['prefix'], grp, cfg['simbees']['suffix'])
    bee_idx_start = 0
    args.verb     = 1
    orig_nums = range(bee_idx_start,
                      bee_idx_start + cfg['simbees']['nbees'])

    #loc_fname = 'all_animate_objs.csv'
    #}}}

    #{{{ where are the casus?

    # for north virtual arena, this one is right.
    # (and x positions are all that matter for horiz analysis)
    if args.vert_arena:
        N = (+9, +0)
        S = (+9, -9)
        fixed_points   = [N, S]
        vert_span      = N[1] - S[1]
        #vert_threshold = max(0, (vert_span - ctrl_zone_width)/2.0)
        #vert_threshold = max(0, (vert_span - ctrl_zone_width)/2.0)
        vert_threshold = fixed_points[0][1] - (vert_span - ctrl_zone_width)/2.0

    else:
        W = (-9, +9)
        E = (0, +9)
        fixed_points    = [W, E]
        horiz_span      = E[0] - W[0]
        horiz_threshold = max(0, (horiz_span - ctrl_zone_width)/2.0)

    #}}}
    #{{{ 1. read all the time-series data
    print "\n",
    print "=" * 60
    print "[I] reading bee posn data for {} bees, {}-xxx".format(
        cfg['simbees']['nbees'], bee_prefix,)
    print "=" * 60

    bee_data = {}
    for prefix in (bee_prefix, ):
        d, fn, nums = log_proc.read_raw_data(
            path, prefix, orig_nums, bee_idx_start, [],
            suffix='csv', verb=args.verb)
        bee_data[prefix] = dict(data=d, nums=nums, files=fn)
    casu_data = {}
    # 2. find time reference for all data
    start_time = log_proc.find_reference_start_time([bee_data.values()], verb=args.verb)
    dt_start_time = datetime.fromtimestamp(start_time)
    print "#[I] Found latest start time to be:", start_time, "({})".format(
        cbtb.utils.dt_ts_to_readable(dt_start_time))

    l = []
    for D in bee_data.values()[0]['data']:
        l.append(D[0,0])
    print "#[I] spread first to last is {} ({}-{})".format(
            max(l) - min(l), max(l), min(l))


    # it makes more sense to coordinate end-times as well
    __cleanup_big_jumps = False
    sd, times = log_proc.clean_sample_trim_data(
        bee_data[bee_prefix]['data'],
        orig_nums, __cleanup_big_jumps, 1, start_time,
        sample_dt)
    if(cfg['simbees'].get('max_expt_len', 0) > 0):
        # find cutoff point
        sd, times = log_proc.trim_data_to_maxlen(sd, times, args.max_expt_len)
        args.expt_len = times[-1] # record the actual length after the trim
    else:
        args.expt_len = times[-1]


    n_bees = len(orig_nums) - len([])
    dec_thresh = (dec_frac * n_bees)

    #}}}

    if args.vert_arena:
        all_zc = zones.compute_time_in_vert_zones(sd, fixed_points, vert_threshold)

    else:
        all_zc = zones.compute_time_in_horiz_zones(sd, fixed_points, horiz_threshold)
    #}}}

    #{{{ plot the i nitial phase, to align (sync flashes etc)
    # a comparable plot to the fraction of bee pixels data
    # w.l.o.g., use frac on left as the defining characteristic.
    bleft = all_zc[0,:] / float(cfg['simbees']['nbees'])
    _nb = cfg['simbees']['nbees']
    _tu = cbtb.maths.compute_threshold(_nb)
    _tl = _nb - _tu

    coll_decn = ((all_zc[0,:] >= _tu) | (all_zc[0,:] <= _tl))

    _cdi = coll_decn[args.start_index:].mean()
    print "===>> CD  index @ {:.3f}".format(_cdi)

    decisions = [ to_decision(value, _tl, (_tu + _tl)/2., _tu) for value in all_zc[0,args.start_index:] ]
    _rcdi = rcdi(decisions)
    print "===>> RCD  index @ {:.3f}".format(_rcdi)

    osc_i = quanitfy_oscil(bleft, i_start=args.start_index)
    print "--->> OSC index @ {:.3f}".format(osc_i)

    dist_to_avg = 0.5 - bleft[args.start_index:].mean()
    print "--->> Distance to mean @ {:.3f}".format(dist_to_avg)

    print "results; {}; {}; {}; {:.4f}; {:.4f}; {:.4f}; {:.4f}".format(
        os.path.basename(args.conf),
        cfg['rpt'], args.A, _cdi, osc_i, dist_to_avg, _rcdi)

    plt.figure(4); plt.clf()
    data = bleft[args.start_index:]
    data = data-data.mean()
    show_fft(data, ax=None, time_step=1.0, use_helper=True)
    plt.xlim(xmax=0.1)




    if SHOW_SYNCFLASHES:
        plt.figure(6); plt.clf()
        plt.plot(times, bleft)
        plt.ylabel('frac bees on left')

        if show_sync:
            for row in sync:
                # TODO: verify this offset usage is ok.
                plt.axvline(
                    row[0]-start_time - float(args.vid_offset),
                    #row[0]-start_time - float(cfg['simbees']['vid_offset']),
                    c='k', ls=':')
    #}}}

    #{{{ show IR sensor data at beginning 30 sec, sync flash to beyond the bee in +5 sec
    if args.casu_inspect is not None:
        c = args.casu_inspect
        plt.figure(8); plt.clf()
        fig, ax = plt.subplots(6, 1, sharex=True, num=8);
        dirs = ['F', 'FL', 'BL', 'B', 'BR', 'FR',]
        ir = irs[c]
        thr = thrs[c]
            #hits = (ir[:, 1:] > thr).sum(axis=1)
        for i in xrange(6):
            ax[i].plot(ir[:,0] - sync_start_time, ir[:, i+1])
            # show some detail on the IR levels
            I = ir[:, i+1]
            hits = I>thr[i]
            if hits.sum() > 0:
                for l in np.percentile(I[hits], [0, 50, 95]):
                    ax[i].axhline(l, c='r')
                        #[10,50,90,95,99,100])
            #ax[i].axhline(thr[i], c='r') # treshold
            # mean value when hit
            # max value
                ax[i].set_ylim(np.percentile(I[~hits], 33),
                            np.percentile(I[hits], 96))

            ax[i].text(0.9,  0.9, dirs[i], transform=ax[i].transAxes, va='top')
            ax[i].text(0.05, 0.9, dirs[i], transform=ax[i].transAxes, va='top')

            for row in sync[:4]:
                ax[i].axvline(
                    #row[0] - sync_start_time  - float(cfg['simbees']['vid_offset']),
                    row[0] - sync_start_time,
                    ymax=0.7, c='k', ls=':')

        plt.xlim(19, 73)
        plt.suptitle('casu {}'.format(casus[c]))
        plt.tight_layout()

    #}}}

    if 0:
        # print some timing info to help with alignment
        print "[I] reference time is   {} ({})".format(
            sync_start_time, cbtb.utils.ux_ts_to_readable(sync_start_time))

        # start time of each casu log
        for i in xrange(len(irs)):
            ir = irs[i]
            cn = casus[i]
            i_start = ir[0,0]
            print "start time of casu {} is {} ({}). Offset: {:.2f}".format(
                cn, i_start,
                cbtb.utils.ux_ts_to_readable(i_start),
                i_start - sync_start_time,
            )

    plt.figure(11); plt.clf()
    thr_u = cbtb.maths.compute_threshold(cfg['simbees']['nbees'])
    thr_l = cfg['simbees']['nbees'] - thr_u

    #bleft = binom_zc[0,:] / float(cfg['simbees']['nbees'])
    plt.plot(times, bleft, label='bees in simln')
    plt.axhline((thr_u-0.5)/float(cfg['simbees']['nbees']), ls='--', c='k')
    plt.axhline((thr_l+0.5)/float(cfg['simbees']['nbees']), ls='--', c='k')

    if args.vert_arena:
        plt.ylabel('frac bees on top')
    else:
        plt.ylabel('frac bees on left')
    plt.legend(loc='upper right')
    plt.ylim(-0.05, 1.05)
    fmtr = process_logs.FuncFormatter(process_logs.min_sec_fmtr)
    ax = plt.gca()
    ax.xaxis.set_major_formatter(fmtr)
    plt.xlabel('time (m:ss)')


    #{{{ plot figure that shows more detail from the quantification of oscil
    plt.figure(5); plt.clf()
    d_sample = bleft[args.start_index:]
    t_sample = times[args.start_index:]
    plt.plot(t_sample, d_sample)
    dc_comp = d_sample.mean()
    plt.ylim(-0.05, 1.05)
    fmtr = process_logs.FuncFormatter(process_logs.min_sec_fmtr)
    ax = plt.gca()
    ax.xaxis.set_major_formatter(fmtr)
    plt.xlabel('time (m:ss)')

    plt.axhline((thr_u-0.5)/float(cfg['simbees']['nbees']), ls='--', c='k')
    plt.axhline((thr_l+0.5)/float(cfg['simbees']['nbees']), ls='--', c='k')
    plt.axhline(dc_comp, c='r')
    ax = plt.gca()
    trans = ax.get_yaxis_transform() # x in axis units; y in data units
    #trans = ax.get_xaxis_transform()
    ax.annotate("mean: {:.2f}".format(dc_comp), xy=(1.01, dc_comp), xycoords=trans)

    #}}}




    #{{{ filtered zone plot
    if 0:
        Wn = .20; N=10; B, A = signal.butter(N, Wn, output='ba')
        azc_f = []
        for zc in all_zc:
            zc_f = signal.filtfilt(B, A, zc)
            azc_f.append(zc_f)

        azc_f = np.array(azc_f)
        #azc_f = np.array(all_zc)

        n_colors = ((0.89, 0.13, 0.090), (0.98, 0.72, 0.099), (0.086, 0.34, 0.976))
        plt.figure(2); plt.clf()
        fig, axx = plt.subplots(1,1,num=2, ) #sharex=True)
        axx = [axx,]
        # use top frame to show the fraction of bees in either/each zone
        if args.todraw['metrics_v_time']:
            zones.pre_filtered_zone_stackplot(
                axx[0],
                #azc_f,
                azc_f[::-1,:],
                times, horiz_threshold, n_bees,
                #colors=n_colors,
                colors=n_colors[::-1],
                sample_dt=sample_dt, horiz_span=horiz_span,
                stack_plot_avail=True,
            )

            #plt.tight_layout()
        #}}}

    #{{{ show where the virtual bees were
    ax = show_posns(sd, i_start=args.start_index, fignum=12)


    #}}}


    #{{{ emit correlation data
    if args.emit_summary:
        t_start = int(4 * 60)
        t_end   = int(19.5 * 60)
        # verify data exists
        sim_s = np.where(times == t_start)[0][0]
        sim_e = np.where(times == t_end)[0][0]

        _save_sd = bleft[sim_s:sim_e]

        D_out = np.zeros((t_end-t_start,))
        for i, t in enumerate(xrange(t_start, t_end)):
            #D_out[i] = [t, _save_vd[i], _save_sd[i]]
            D_out[i] = [t, _save_sd[i]]

        if 0:
            # write out the file
            pth = "/home/rmm/Dropbox/assisi/Y4/socinteg-data/linked_vr/"
            exptname = os.path.basename(cfg['base'].rstrip('/'))
            logdir = os.path.join(pth, exptname)
            cbtb.utils.mkdir_p(logdir)

            # write header
            h = "#sec, vid_frac_left, sim_frac_left"
            with open( os.path.join(
                logdir, "summary-{}.csv".format(grp)),
                'w') as f:
                f.write(h + "\n")
                np.savetxt(f, D_out)

                print "[I] wrote to ", f.name

    #}}}



