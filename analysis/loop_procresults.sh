#!/bin/sh

cfg=$1
rpt_max=${2:-6}  # defaults to 6

if [ ! -f ${cfg} ] ; then
    echo "[W] ${cfg} does not exist. Try again!"
else
    for r in `seq 1 ${rpt_max}`; do 
        for a in 0 1; do
            python just_simln.py --conf ${cfg} -a $a -r $r | grep results ; 
        done ; 
    done
fi


