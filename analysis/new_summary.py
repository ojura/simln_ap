import numpy as np
import matplotlib.pyplot as plt

plt.ion()


# load data
#Daniel  12 bees, 120 samples, richer      150/150 x 3       rgr_90min_12b_120avg_150s_150o
#Daniel  12 bees, 120 samples, richer      150/000 x 3       rgr_90min_12b_120avg_150s_000o
#Juraj   12 bees,  20 samples, richer      150/150 x 4       rgr_90min_12b_020avg_150s_150o
#Juraj   12 bees,  20 samples, oscillation 150/150 x 4 / 3?  oscil_90min_12b_020avg_150s_150o
#Rob     12 bees, 240 samples, oscillation 150/150 x 6       oscil_90min_12b_240avg_150s_150o


cols = range(2,8)
o240       = np.loadtxt("data/90m/oscil_90min_12b_240avg_150s_150o.csv", delimiter=';', usecols=cols)
r120_withx = np.loadtxt("data/90m/rgr_90min_12b_120avg_150s_150o.csv", delimiter=';', usecols=cols)
r120_nox   = np.loadtxt("data/90m/rgr_90min_12b_120avg_150s_000o.csv", delimiter=';', usecols=cols)

labels = ["o240", "r120_with_x", "r120_no_x"]
XX = [o240, r120_withx, r120_nox]

# columns are: <flag> <config file> rpt; arena; CDI | OSCI | dist to avg| RCDI
CDI  = [x[:,2] for x in XX]
OSCI = [x[:,3] for x in XX]
d2av = [x[:,4] for x in XX]
RCDI = [x[:,5] for x in XX]


plt.figure(1); plt.clf()
plt.boxplot(CDI, labels=labels)
plt.title('collective decsns (metric 1)')

plt.figure(2); plt.clf()
plt.boxplot(RCDI, labels=labels)
plt.title('collective decsns (metric 2)')

plt.show()
