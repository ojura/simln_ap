#!/usr/bin/env python
# -*- coding: utf-8 -*-

from assisipy import bee
import time, argparse
from assisipy_utils.mgmt import specs
import numpy as np
import matplotlib.pyplot as plt


#{{{ PoplnObserver
class PoplnObserver(object):
    def __init__(self, agent_listing, ts_max=1000):
        agents = specs.read_agent_handler_data_yaml(agent_listing, ty_filter='bee')
        self.agents = {}
        self.ts_max = ts_max
        for a in agents:
            name = a.get('name')
            self.agents[name] = {
                'pub_addr': a.get('pub_addr'),
                'sub_addr': a.get('sub_addr'),
                'init_pos': a.get('pose')
            }

        print "[I] loaded info for {} agents".format(len(self.agents.keys()))

    def connect_to_agents(self):
        '''
        attempt to connect to all agents in loaded list
        '''
        self.handles = {}
        self.hist = {}
        for name, data in self.agents.items():
            h = bee.Bee(name=name, sub_addr=data['sub_addr'], pub_addr=data['pub_addr'],)
            if h is not None:
                self.handles[name] = h
                self.hist[name] = np.zeros((self.ts_max, 4))

        self.start_time = time.time()
        self.ts = 0


    def update_locations(self):
        now = time.time()
        for name in self.handles.keys():
            x, y, yaw = self.handles[name].get_true_pose()
            self.hist[name][self.ts] = [now, x, y, yaw]

        self.ts += 1


    def cumulative_dist(self, name):
        '''
        compute the total distance travelled during watching period
        for agent "name"

        '''
        data = self.hist[name]
        d0 = data[0,:]
        drecent = data[self.ts-1,:]
        dt = drecent[0] - d0[0]
        x = data[0:self.ts-1,1]
        y = data[0:self.ts-1,2]

        dist =  np.sqrt(np.diff(x)**2 + np.diff(y)**2)
        #length=[0;cumsum(sqrt(diff(x(:)).^2 + diff(y(:)).^2))];
        #dist = np.sqrt( np.dot(x, x) - (np.dot(x, y) + np.dot(x, y)) + np.dot(y, y))

        return dt, dist.sum()

#}}}








# ok that is quite easy. So let's do this for a series of points.

if __name__ == "__main__":
    # try connecting to bee
    # and then periodically report where it is
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--conf-file', type=str, default=None)
    args = parser.parse_args()

    imax = 100
    hdlr = PoplnObserver(agent_listing="/tmp/test_listing.csv", ts_max=imax)
    #hdlr = PoplnObserver(agent_listing="/tmp/short_lst.csv")
    hdlr.connect_to_agents()


    if 1:
        i = 0
        # run handler until keyboard interrupt. (or other SIGINT)
        try:
            while True:
                time.sleep(0.25)
                hdlr.update_locations()
                i += 1
                if i >= imax:
                    break

                if i % 5 == 0:
                    # report a bit
                    print "[I] after {} updates:".format(i)
                    for name, data in hdlr.hist.items():
                        d0 = data[0,:]
                        drecent = data[i-1,:]
                        dt = drecent[0] - d0[0]
                        dt2, dx = hdlr.cumulative_dist(name)
                        print "  {:15}   {:6.1f}| {:6.1f}. Moved {:.2f}cm ({:.1f}cm/s)".format(
                            name, drecent[1], drecent[2], dx, dx/dt2)
                        #print "  {:15} {:5.2f}s  {:.1f}|{:.1f}".format(
                        #    name, dt, drecent[1], drecent[2])


                    print " "

                    # good



        except KeyboardInterrupt:
            print "disconnecting the observer after {} observations.".format(i)


    # show an image of their trajectories.
    plt.figure(1); plt.clf()
    for name, data in hdlr.hist.items():
        dst = np.sqrt(np.ediff1d(data[:,1])**2 + np.ediff1d(data[:,2])**2).sum()
        plt.plot(data[:,1], data[:,2], label="{}: {:.2f}cm".format(name, dst))


    plt.legend(loc='best')
    plt.axis('equal')
    plt.show()





