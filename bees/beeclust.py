#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
controlling of the bees, implementing BEECLUST behaviour that is sensitive to
heat in environment (and social interaction).


'''

from assisipy import bee
import os, yaml, argparse
import bee_log
import datetime, time
import random
from math import pi
import numpy as np

#{{{ notes
'''
model, taking into account
- obstacles
- social influence
- heat
- airflow

if not colliding:
    if not airflow:
        fwd()
    else: # airflow
        turn()

    else: # collision
        if physical obstacle:
            turn()
        else: # bee obstacle
            T <- sense environment here
            wait_func(T)


'''
#}}}

IMMEDIATE_FWD = False
START_STATE_FWD = True
DETECT_SLIDE = True # bit of a nightmare to test with teleporting-based reset
#DETECT_SLIDE = False # bit of a nightmare to test with teleporting-based reset
WAIT_MAX = 25.0
AGIT_FADE = 2.5 # seems approx sufficient to leave a 5cm airflow, mostly.
#TS_SCALING = 0.05 # testing the push behavior
TS_SCALING = 3.0 # simulator time isn't as fast as realtime
BODY_OLAP = 0.3

#{{{ behaviour enum
class BeeBehavState(object):
    '''
    an enumeration type for behaviours
    '''
    WALK             = 0
    PAUSE_BEE        = 1
    TURN_POST_PAUSE  = 2
    TURN_AVOID_WALL  = 3
    TURN_AVOID_AIR   = 4
    TURN_AVOID_SLIDE = 5
    INIT = -1



#}}}

#{{{ math convenience
def deg2rad(x):
    '''
    convert an angle in degrees to an angle in radians. Pure python
    implementation as per the numpy docs. (scalar only - not vectorised)
    '''
    return x * (pi / 180.0)

def rad2deg(x):
    '''
    convert an angle in radians to an angle in degrees. Pure python
    implementation as per the numpy docs. (scalar only - not vectorised)
    '''
    return x * (180.0 / pi)
#}}}

#{{{ waiting time functions
def compute_wait_hill(temp,
        a=   3.090475362,
        b=   -0.0402754316,
        c=   -28.4570871346,
        d=   1.7850341689,
        e=   22.4490966476,
        f=   0.6451864513,
        ):
    '''
    compute waiting time for bee according to temperature, based on experimental data
    from Graz lab
    =((a+b*D5)^c_/((a+b*D5)^c_+d^c_))*e+f

    Hill function, fitted for range 0..44

    '''
    temp = float(temp)

    waiting_time = ((a+b*temp)**c/((a+b*temp)**c+d**c))*e+f
    #waiting_time = ( \
    #        (a + b * temp) ** c /( (a + b * temp) **c + d**c ) \
    #        ) * e + f

    return waiting_time
#}}}

#{{{ BeeClustBee
class BeeClustBee(object):
    '''
    implementation of the behavoural model BeeClust as per Schmickl 2008.
    using Enki/assisipy bee interface; including extension to agitation by air.
    '''
    #{{{ initialiser
    def __init__(self, bee_name, logfile, pub_addr, sub_addr, conf_file=None,
                 verb=False):
        self.bee_name = bee_name
        self.state = BeeBehavState.INIT
        if conf_file is not None:
            with open(conf_file, 'r') as f:
                conf = yaml.safe_load(f)
        else:
            conf = {}

        '''instantiates a assisipy.bee object but not derived from bee (At present)
        # default settings are 2nd argument, if not set by config file
        '''
        # cm - how close is a detected object considered a collision?
        self.range_thresh_any_collision = conf.get(
                'range_thresh_any_collision', 0.25)
                #'range_thresh_any_collision', 0.1 +BODY_OLAP)
        self.override_fwd_clr           = conf.get('override_fwd_clr', False)
        self._fwd_clr                   = conf.get('fwd_clr', (0.3,0.3,0.3))
        self.update_delay             = 0.25 #  = conf.get('update_interval', 0.1)
        self.airflow_agit_thresh        = conf.get('airflow_agit_thresh', 2.0)
        self.fwd_speed                  = conf.get('fwd_speed', 2.5)
        self.slide_det_len = 3
        self.slide_yaw_tol = deg2rad(5)

        self.range_thresh_bee = 0.25
        self.verb = verb

        print "attempting to connect to bee with name %s" % bee_name
        print "\tpub_addr:{}".format(pub_addr)
        print "\tsub_addr:{}".format(sub_addr)
        self.mybee = bee.Bee(name=self.bee_name, pub_addr=pub_addr,
                             sub_addr=sub_addr)
        self.counter = 0
        self.logger = bee_log.LogBeeActivity(bee_to_log=self.mybee,
                logfile=logfile, append=False)

        #self.CLR_FWD  = {'r':0.93,'g':0.79,'b'r:0}
        #self.CLR_WAIT = {'r':0.93,'g':0.0, 'b'r:0}
        self.CLR_FWD  = (0.93, 0.79, 0)
        self.CLR_COLL_OBJ  = (0, 0, 1)
        self.CLR_SLIDE     = (0, 0, 1) #(0, 0.5, 0.5)
        self.CLR_AGIT_AVD  = (1, 1, 1)
        self.CLR_COLL_BEE  = (0, 1, 0)
        self.CLR_WAIT  = (0.93, 0.0, 0)
        self._xhist   = np.zeros(self.slide_det_len,)
        self._yhist   = np.zeros(self.slide_det_len,)
        self._yawhist = np.zeros(self.slide_det_len,)
        self.agitated_refac = time.time()


        if self.override_fwd_clr:
            # special color for this machine / bee
            c = [float(n) for  n in self._fwd_clr.strip('()').split(',')]
            self.CLR_FWD = c
            #print "[I] custom color for bee %s" % bee_name,  self.CLR_FWD, len(self.CLR_FWD)
            #self.CLR_FWD = (0.0, 0.60, 0.80)


    #}}}
    #{{{ detect_sliding
    def detect_sliding(self):
        '''
        return true if have been sliding, where bearing does not get honoured
        '''
        sliding = False
        x, y, yaw = self.mybee.get_true_pose()
        # step all data back by one
        self._xhist   = np.roll(self._xhist, 1)
        self._yhist   = np.roll(self._yhist, 1)
        self._yawhist = np.roll(self._yawhist, 1)
        # enter new recordings
        self._xhist[0]   = x
        self._yhist[0]   = y
        self._yawhist[0] = np.mod(yaw, 2*np.pi)


        # for VERTICAL walls, these are heading at pi/2 or -pi/2
        #0.75 * np.sin(np.deg2rad(5))
        if (abs(yaw - pi/2.0) > self.slide_yaw_tol or
                abs(yaw - (3.0*pi)/2.0) > self.slide_yaw_tol):
            # what is x movement?
            d = []
            for i in xrange(len(self._xhist) - 1):
                d.append(self._xhist[i] - self._xhist[i+1])

            mean_xmov = np.mean(np.abs(d))


            #print "[I] cehcking for V  slide with truepos: {:.2f}, {:.2f}, {:.2f} (move: {:.2f}".format( x,y, np.rad2deg(yaw), mean_xmov),
            if mean_xmov < (0.05 * np.sin(np.deg2rad(5))):
                sliding = True
                #print "AN I THINK I AM"
            #else: print "(not slide)"

        # for HIRIIZONTAL walls, these are 0 or pi
        # note - both tests should be made independently -- not elif here
        if (abs(yaw - pi) > self.slide_yaw_tol or
                abs(yaw - 0) > self.slide_yaw_tol):
            # what is y movement?
            d = []
            for i in xrange(len(self._yhist) - 1):
                d.append(self._yhist[i] - self._yhist[i+1])
            mean_ymov = np.mean(np.abs(d))

            #print "[I] cehcking for H  slide with truepos: {:.2f}, {:.2f}, {:.2f} (move: {:.2f}".format( x,y, np.rad2deg(yaw), mean_xmov),
            if mean_ymov < (0.05 * np.sin(np.deg2rad(5))):
                sliding = True
                #print "AN I THINK I AM"
            #else: print "(not slide)"




        return sliding
    #}}}

    #{{{ detect_collision
    def detect_collision(self, phys_fwd_only=True):
        '''
        check if we are going to collide into anything. Returns two
        booleans:
        bee_nearby  : true if a bee is within range on any sensor
        obj_collide : true if there is not space in front to continue
                      (regardless of object type)

        Thus, the truth table is as follows
            b   o   significance
            =================================================
            0   0   can move forwards; no social influence here
            1   0   social influence here (infer it is to side; room to move)
            0   1   cannot move forwards; non-bee object blocking
            1   1   bee directly in front.

        Typically the actions will be considered are:
        1x => social influence dominates
        00 => continue forwards
        01 => need to avoid obstacle

        '''
        # measure ranges and object types
        o_l, r_l = self.mybee.get_object_with_range(bee.OBJECT_LEFT_FRONT)
        o_f, r_f = self.mybee.get_object_with_range(bee.OBJECT_FRONT)
        o_r, r_r = self.mybee.get_object_with_range(bee.OBJECT_RIGHT_FRONT)

        # default results
        bee_nearby  = False
        obj_collide = False
        if (
                (o_l == 'Bee' and r_l < self.range_thresh_bee) or
                (o_f == 'Bee' and r_f < self.range_thresh_bee) or
                (o_r == 'Bee' and r_r < self.range_thresh_bee) ):
            bee_nearby = True

        if phys_fwd_only:
            if r_f < self.range_thresh_any_collision:
                obj_collide = True
        else:
            if (
                    (o_l != 'Bee' and r_l < self.range_thresh_any_collision) or
                    (o_f != 'Bee' and r_f < self.range_thresh_any_collision) or
                    (o_r != 'Bee' and r_r < self.range_thresh_any_collision) ):
                obj_collide = True
        if 0:
          if ( 1 or
                (r_l < self.range_thresh_any_collision) or
                (r_f < self.range_thresh_any_collision) or
                (r_r < self.range_thresh_any_collision) ):
            print "L {:4.2f} {:8} | F {:4.2f} {:8} | R {:4.2f} {:8}. Bee? {:6} Obj? {:6}".format(
                    r_l, o_l, r_f, o_f, r_r, o_r,
                    bee_nearby, obj_collide)



        return bee_nearby, obj_collide
    #}}}

    #{{{ airflow
    def detect_airflow(self):
        '''
        The least assumptions model here only pays attention to
        airflow intensity.  If above the individual threshold, it
        returns true.
        '''
        airflow_intensity = self.mybee.get_airflow_intensity ()
        #airflow_angle     = self.mybee.get_airflow_direction ()
        return (airflow_intensity > self.airflow_agit_thresh), airflow_intensity
    #}}}

    #{{{ basic motion behaviours/components
    def stop(self):
        self.mybee.set_color(r=0.25, g=0.25, b=0.25)
        self.mybee.set_vel(0, 0)
    def go_straight(self):
        vel = self.fwd_speed
        #vel = 2.5 # cm/s  - real mean vel = 1.25
        self.mybee.set_vel(vel, vel) # speed of L/R wheels

    def turn_random(self, k=1.5, mn=.25, mx=2.0):
        vel = 0.4 * k # cm/s
        direction = random.choice([-1,1]) # pick a sign of velocity
        self.mybee.set_vel(vel * direction, -1 * vel * direction)
        t = random.uniform((1/k)*mn, (1/k)*mx)
        time.sleep(t)
        return t

    def measure_heat(self):
        ''' measure the temperature in current position '''
        temp = self.mybee.get_temp()
        return temp
   #}}}

    #{{{ interruptible_sleep
    def interruptible_sleep(self, waiting_time):
        '''
        sleep for `waiting_time` seconds, unless woken up by airflow.

        '''
        started = time.time()
        steps = int(float(waiting_time)  / self.update_delay)
        for i in xrange(steps):
            time.sleep(self.update_delay) # no more than 10x /sec updates
            airflow_intensity = self.mybee.get_airflow_intensity ()
            if (airflow_intensity > self.airflow_agit_thresh):
                if 1 or self.verb: print "[I] {} got interrupted after {:.1f}/{:.1f} sleep".format(
                        self.bee_name, time.time() - started, waiting_time)
                self.mybee.set_color(*self.CLR_AGIT_AVD)
                waited = time.time() - started
                return False, waited
                break
        waited = time.time() - started
        return True, waited
    #}}}

    def behav_heat_air(self):
        '''
        explicit object aversion; social interaction (heat-modulated);
        agitation by air
        '''
        #{{{ beeclust model loop
        if not START_STATE_FWD:
            self.state = BeeBehavState.INIT
            self.logger.record_pos(behav=self.state, start=True)
            _t = self.turn_random() # in case the bee got stuck on wall at init time?
            self.logger.record_pos(behav=self.state, start=False, decnParams=[_t,])

        while True:
            now = time.time()
            #TODO: assign state here! Possible state entries include:
            # - walk ***
            # + init (which was a turn, and already logged end)
            # #- sleep (either full time waited, or partial since interrupted; for
            # #         latter condition, there will be a difference between actual
            # #         and desired time waited)
            # #  [exits with a turn]
            # - after turn, from:
            #    + air agitation
            #    + after sleep
            #    + avoiding collision with wall
            # do any of these actually need logging?
            if self.state == BeeBehavState.WALK:
                self.logger.record_pos(behav=self.state, start=False)
            time.sleep(self.update_delay) # no more than 10x /sec updates

            # test environment nearby
            bee_nearby, collide_fwd = self.detect_collision(False)
            am_sliding = self.detect_sliding()
            air_agitation, _air_rate = self.detect_airflow()
            # agitated state transition
            #if self.agitated_refac > now:

            # behavioural logic
            if self.agitated_refac < now and air_agitation:
                self.state = BeeBehavState.TURN_AVOID_AIR
                self.mybee.set_color(*self.CLR_AGIT_AVD)
                self.logger.record_pos(behav=self.state, start=True, decnParams=[_air_rate, self.agitated_refac-now])

                self.turn_random()
                now = time.time()
                self.agitated_refac = now + AGIT_FADE;
                self.logger.record_pos(behav=self.state, start=False)

            else:
                if bee_nearby:
                    self.mybee.set_color(self.CLR_COLL_BEE)
                    self.state = BeeBehavState.PAUSE_BEE
                    current_temp = self.measure_heat()
                    waiting_time = TS_SCALING * compute_wait_hill(current_temp)
                    self.logger.record_pos(behav=self.state, start=True, decnParams=[waiting_time])
                    if self.verb :
                        xx, yy, _ = self.mybee.get_true_pose()
                        print "{4} Detected bee@{2:+.1f}{3:+.1f}; curent temp is {0:.2f}->waiting time {1:.1f} ".format(
                                current_temp, waiting_time, xx, yy,
                                self.bee_name, )
                    self.stop()
                    self.mybee.set_color(r=0.35+0.65*(waiting_time/WAIT_MAX*TS_SCALING), g=0, b=0)
                    _, waited = self.interruptible_sleep(waiting_time)
                    self.logger.record_pos(behav=self.state, start=False,
                            decnParams=[waiting_time, waited])

                    # after the pause, random turn before fwd again
                    self.state = BeeBehavState.TURN_POST_PAUSE
                    self.logger.record_pos(behav=self.state, start=True, )
                    _t = self.turn_random(mn=.15, mx=.6) # (TODO: WHY??????)
                    self.logger.record_pos(behav=self.state, start=False, decnParams=[_t,])

                elif collide_fwd:
                    self.state = BeeBehavState.TURN_AVOID_WALL
                    self.mybee.set_color(*self.CLR_COLL_OBJ)
                    self.logger.record_pos(behav=self.state, start=True, )
                    _t = self.turn_random()
                    self.logger.record_pos(behav=self.state, start=False, decnParams=[_t,])

                else: # not agitated, no bees, no collisions => go fwd
                    self.mybee.set_color(*self.CLR_FWD)
                    if DETECT_SLIDE and am_sliding:
                        self.state = BeeBehavState.TURN_AVOID_SLIDE
                        self.mybee.set_color(*self.CLR_SLIDE)
                        self.logger.record_pos(behav=self.state, start=True, )
                        _t = self.turn_random()
                        self.logger.record_pos(behav=self.state, start=False, decnParams=[_t,])

                    self.state = BeeBehavState.WALK
                    self.mybee.set_color(*self.CLR_FWD)
                    self.logger.record_pos(behav=self.state, start=True, )
                    self.go_straight()

        #}}}

#}}}


if __name__ == "__main__":

    #{{{ cmd line args
    parser = argparse.ArgumentParser()
    parser.add_argument('--logpath', type=str, required=True,
                        help="path to record output in")
    parser.add_argument('-bn', '--bee-name', type=str, default='BEE',
                        help="name of the bee to attach to")
    parser.add_argument('-pa', '--pub-addr', type=str, default='tcp://127.0.0.1:5556',
                        help="publish address (wherever the enki server listens for commands)")
    parser.add_argument('-sa', '--sub-addr', type=str, default='tcp://127.0.0.1:5555',
                        help="subscribe address (wherever the enki server emits commands)")
    parser.add_argument('-c', '--conf-file', type=str, default=None)
    parser.add_argument('-v', '--verb', action='store_true', help="be verbose")

    args = parser.parse_args()
    #}}}

    random.seed()
    logfile = os.path.join(args.logpath, "bee_track-{}.csv".format(args.bee_name))
    the_bee = BeeClustBee(
        bee_name=args.bee_name, logfile = logfile,
        pub_addr=args.pub_addr, sub_addr=args.sub_addr,
        conf_file=args.conf_file,
        verb=args.verb,
    )

    try:
        #print "PID: ", os.getpid()
        while True:
            time.sleep(0.05)
            the_bee.behav_heat_air()
    except KeyboardInterrupt:
        print "shutting down bee {}".format(args.bee_name)
        the_bee.stop()
        s = "# {} Finished at: {}".format(
                the_bee.bee_name, datetime.datetime.fromtimestamp(time.time()))
        the_bee.logger.logfile.write(s + "\n")
        the_bee.logger.logfile.close()

